import java.util.Map.Entry;

public interface ShoppingListItem {

	public String getItemName();
	public Double getQuantity();
	public Entry<Double, Long> getPricePerQuantity();
	public Long getTotalPrice();
	
	public void setItemName(String s);
	public void setQuantity(Double d);
	public void setPricePerQuantity(Entry<Double, Long> e);
	
}
