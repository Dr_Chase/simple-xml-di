import java.util.Map.Entry;

public class PersonalShoppingListItem implements ShoppingListItem {
	
	private String itemName;
	private Double quantity;
	private Entry<Double, Long> pricePerQuantity;
	
	

	public PersonalShoppingListItem() {
		super();
	}

	public PersonalShoppingListItem(String itemName, Double quantity, Entry<Double, Long> pricePerQuantity) {
		super();
		this.itemName = itemName;
		this.quantity = quantity;
		this.pricePerQuantity = pricePerQuantity;
	}

	public String getItemName() {

		return itemName;
	}

	public Double getQuantity() {

		return quantity;
	}

	public Entry<Double, Long> getPricePerQuantity() {

		return pricePerQuantity;
	}

	public Long getTotalPrice() {

		return Math.round( quantity * (pricePerQuantity.getValue() / pricePerQuantity.getKey()) );
	}

	public void setItemName(String s) {
		// TODO Auto-generated method stub
		itemName = s;
	}

	public void setQuantity(Double d) {
		// TODO Auto-generated method stub
		quantity = d;
	}

	public void setPricePerQuantity(Entry<Double, Long> e) {
		// TODO Auto-generated method stub
		pricePerQuantity = e;
	}

	@Override
	public String toString() {
		return "PersonalShoppingListItem [itemName=" + itemName + ", quantity=" + quantity + ", pricePerQuantity="
				+ pricePerQuantity + "]";
	}

	
}
