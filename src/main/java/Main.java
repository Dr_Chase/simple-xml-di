import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");
		
		ShoppingListItem listItem = context.getBean("shoppingListItem", PersonalShoppingListItem.class);
		
		if( listItem == null)	{
			System.out.println("listItem null, daaaaaaang");
		} else	{
			System.out.println(listItem);
		}
	}

}
