import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.Test;

public class ListItemTest {

	@Test
	public void testBean()	{
		
		ApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");
		
		ShoppingListItem listItem = context.getBean("shoppingListItem", PersonalShoppingListItem.class);
		
		assertNotNull(listItem);
		assertEquals("kola", listItem.getItemName());
	}
}
